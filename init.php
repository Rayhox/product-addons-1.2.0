<?php
/**
 * Plugin Name: YITH Products Addons
 * Description: Plugin that helps you creates custom addons to products pages.
 * Version: 1.0.0
 * Author: Héctor García
 * Author URI: https://yithemes.com/
 * Text Domain: yith-products-addons
 */

! defined( 'ABSPATH' ) && exit;

if ( ! defined( 'YITH_PA_VERSION' ) ) {
	define( 'YITH_PA_VERSION', '1.0.0' );
}

if ( ! defined( 'YITH_PA_DIR_URL' ) ) {
	define( 'YITH_PA_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'YITH_PA_DIR_ASSETS_URL' ) ) {
	define( 'YITH_PA_DIR_ASSETS_URL', YITH_PA_DIR_URL . 'assets' );
}

if ( ! defined( 'YITH_PA_DIR_ASSETS_CSS_URL' ) ) {
	define( 'YITH_PA_DIR_ASSETS_CSS_URL', YITH_PA_DIR_ASSETS_URL . '/css' );
}

if ( ! defined( 'YITH_PA_DIR_ASSETS_JS_URL' ) ) {
	define( 'YITH_PA_DIR_ASSETS_JS_URL', YITH_PA_DIR_ASSETS_URL . '/js' );
}

if ( ! defined( 'YITH_PA_DIR_PATH' ) ) {
	define( 'YITH_PA_DIR_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'YITH_PA_DIR_INCLUDES_PATH' ) ) {
	define( 'YITH_PA_DIR_INCLUDES_PATH', YITH_PA_DIR_PATH . '/includes' );
}

if ( ! defined( 'YITH_PA_DIR_TEMPLATES_PATH' ) ) {
	define( 'YITH_PA_DIR_TEMPLATES_PATH', YITH_PA_DIR_PATH . '/templates' );
}

if ( ! defined( 'YITH_PA_DIR_VIEWS_PATH' ) ) {
	define( 'YITH_PA_DIR_VIEWS_PATH', YITH_PA_DIR_PATH . 'views' );
}



! defined( 'YITH_PA_INIT' ) && define( 'YITH_PA_INIT', plugin_basename( __FILE__ ) );
! defined( 'YITH_PA_SLUG' ) && define( 'YITH_PA_SLUG', 'yith-products-addons' );
! defined( 'YITH_PA_SECRETKEY' ) && define( 'YITH_PA_SECRETKEY', 'zd9egFgFdF1D8Azh2ifA' );
! defined( 'YITH_PA_OPTIONS_PATH' ) && define( 'YITH_PA_OPTIONS_PATH', YITH_PA_DIR_PATH . 'plugin-options' );

/**
 * Include the scripts
 */
if ( ! function_exists( 'yith_pa_init_classes' ) ) {

	/**
	 * Yith_pa_init_classes
	 *
	 * @return void
	 */
	function yith_pa_init_classes() {

		load_plugin_textdomain( 'yith-personalize-products', false, basename( dirname( __FILE__ ) ) . '/languages' );

		require_once YITH_PA_DIR_INCLUDES_PATH . '/class-yith-pa-products-addons.php';

		if ( class_exists( 'YITH_PA_Products_Addons' ) ) {
			/*
			*	Call the main function
			*/
			yith_pa_products_addons();
		}
	}
}


add_action( 'plugins_loaded', 'yith_pa_init_classes', 11 );
