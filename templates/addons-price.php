<?php 
/** Add ons container */

?>

<div class="yith-pa-addon-container">
	<div class="yith-pa-variation-addons"></div>
	<?php
	foreach ( $addons as $addon ) {

		if ( isset( $addon['enabled'] ) && 'yes' === $addon['enabled'] ) {
			$addon['product_id'] = $product->get_id();
			wc_get_template( 'print-addon.php', compact( 'addon' ), '', YITH_PA_DIR_TEMPLATES_PATH . '/' );
		}
	}
	?>

	<?php if ( ! ! $addons ) : ?>
	<div class="yith-pa-total-price-container">
		<div class="yith-pa-total-price__title"><?php esc_html_e( 'Price totals', 'yith-products-addons' ); ?></div>
		<div class="yith-pa-total-price__row yith-pa-product-price">
			<div class="yith-pa-product-price__text"><?php esc_html_e( 'Product price', 'yith-products-addons' ); ?></div>
			<div class="yith-pa-product-price__value price"><?php print_price_on_addon( floatval( $product->get_price() ) ); ?></div>
		</div>
		<div class="yith-pa-total-price__row yith-pa-addon-price">
			<div class="yith-pa-addons-price__text"><?php esc_html_e( 'Additional options price', 'yith-products-addons' ); ?></div>
			<div class="yith-pa-addons-price__value price"><?php print_price_on_addon( 0 ); ?></div>
		</div>
		<div class="yith-pa-total-price__row yith-pa-total-price">
			<div class="yith-pa-total-price__text"><?php esc_html_e( 'Total', 'yith-products-addons' ); ?></div>
			<div class="yith-pa-total-price__value price"><?php print_price_on_addon( 0 ); ?></div>
		</div>
	</div>
	<?php endif; ?>
</div>
