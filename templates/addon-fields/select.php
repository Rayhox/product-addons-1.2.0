<?php
/** Addon frontend select template */

?>
<div class="yith-pa-addon__desciption">
	<?php echo isset( $description ) ? esc_html( $description ) : ''; ?>
</div>
<select class="yith-pa-addon__input yith-pa-field-select yith-proteo-standard-select" name="yith-pa-field-<?php echo intval( $index ); ?>" id="yith-pa-<?php echo esc_html( $product_id ) . '-' . esc_html( $index ); ?>">
	<?php foreach ( $options as $option ) : ?>
		<option value=" <?php echo esc_attr( $option['name'] . $option['price'] ); ?>">
			<?php
			$option_text = esc_html( $option['name'] );
			echo esc_html( $option_text );
			?>
			</option>
	<?php endforeach; ?>
</select>
