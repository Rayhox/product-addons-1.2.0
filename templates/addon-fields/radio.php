<?php
/** Addon frontend radio template */

$checked = 0;
?>

<div class="yith-pa-addon__desciption">
	<?php echo isset( $description ) ? esc_html( $description ) : ''; ?>
</div>
<?php foreach ( $options as $option ) : ?>
	<label class="yith-pa-addon__radio-label">
		<input type="radio" class="yith-pa-addon__input yith-pa-field--radio" 
			value="<?php echo esc_attr( $option['name'] . $option['price'] ); ?>"
			name="yith-pa-field-<?php echo intval( $index ); ?>"
			id="yith-pa-<?php echo esc_html( $product_id ) . '-' . esc_html( $index ); ?>"
			<?php if ( 0 === $checked ) :
				echo 'checked';
			endif ?>>
		<span class="yith-pa-field--option-text">
			<?php
			$option_text = esc_html( $option['name'] );
			echo esc_html( $option_text ) . ' (+' . get_woocommerce_currency_symbol() . esc_html( $option['price'] . ')' );
			?>
		</span>
	</label>
<?php 
	$checked++;
	endforeach;
?>
