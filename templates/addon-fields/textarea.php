<?php
/** Addon frontend textarea template */

?>
<div class="yith-pa-addon__desciption">
	<?php echo isset( $description ) ? esc_html( $description ) : ''; ?>
</div>
<textarea class="yith-pa-addon__input <?php echo 'free' !== $price_settings ? 'yith-pa-field--text' : ''; ?>"
	name="yith_pa-field-<?php echo intval( $index ); ?>"
	id="yith-pa-<?php echo esc_html( $product_id ) . '-' . esc_html( $index ); ?>">
</textarea>
