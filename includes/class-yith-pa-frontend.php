<?php
/**
 * This file belongs to the YITH Products Addons.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'YITH_PA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PA_Frontend' ) ) {

	/**
	 * YITH_PA_Frontend
	 */
	class YITH_PA_Frontend {

		/**
		 * Main Instance
		 *
		 * @var YITH_PA_Frontend
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PA_Frontend Main instance
		 * @author Héctor García
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_PA_Frotend constructor.
		 */
		private function __construct() {
			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
			// Print addons.
			add_action( 'woocommerce_before_add_to_cart_button', array( $this, 'yith_print_addons' ) );
			// Add addons data into cart.
			add_filter( 'woocommerce_add_cart_item', array( $this, 'yith_add_addons_into_cart' ), 10, 2 );
			add_filter( 'woocommerce_get_item_data', array( $this, 'yith_show_addons_in_cart' ), 10, 2 );
			add_action( 'woocommerce_checkout_create_order_line_item', array( $this, 'yith_add_addons_data_in_order' ), 10, 4 );
			// Add addons data into orders admin page.
			add_action( 'woocommerce_admin_order_data_after_billing_address', array( $this, 'yith_add_addons_data_in_orders_page', 10, 1 ) );
			// Set the custom price based on addons.
			add_filter( 'woocommerce_before_calculate_totals', array( $this, 'yith_add_addons_price_to_product' ), 10, 3 );
			// AJAX for variations.
			add_action( 'wp_ajax_yith_pa_variation_addons', array( $this, 'yith_pa_ajax_variations_addons' ) );
			add_action( 'wp_ajax_nopriv_yith_pa_variation_addons', array( $this, 'yith_pa_ajax_variations_addons' ) );

		}

		/**
		 * Enqueue scripts and styles on frontend
		 *
		 * @return void
		 */
		public function enqueue_scripts() {
			wp_register_style( 'yith-pa-frontend-css', YITH_PA_DIR_ASSETS_CSS_URL . '/yith-pa-frontend.css', array(), YITH_PA_VERSION );
			wp_register_script( 'yith-pa-frontend-js', YITH_PA_DIR_ASSETS_JS_URL . '/yith-pa-frontend.js', array(), YITH_PA_VERSION );
			if ( function_exists( 'is_product' ) && is_product() ) {
				$product = wc_get_product();
				wp_enqueue_style( 'yith-pa-frontend-css' );
				wp_enqueue_script( 'yith-pa-frontend-js' );
				wp_localize_script(
					'yith-pa-frontend-js',
					'yithPa',
					array(
						'decimalSeparator' => wc_get_price_decimal_separator(),
						'decimalPrecision' => wc_get_price_decimals(),
						'productPrice'     => $product->get_price(),
						'ajaxUrl'          => admin_url( 'admin-ajax.php' ),
					)
				);
			}

		}
		/**
		 * Print addons on frontend
		 *
		 * @param  mixed $product Product object.
		 * @return void
		 */
		public function yith_print_addons( $product = false ) {
			$product = wc_get_product( $product );
			if ( ! $product ) {
				global $product;
			}
			if ( ! $product instanceof WC_Product ) {
				return;
			}

			$product_id     = $product->get_id();
			$general_addons = get_option( 'yith_pa-addon' );
			$addons_meta    = $product->get_meta( 'yith_pa-addon' );
			$addons_meta    = is_array( $addons_meta ) ? $addons_meta : array();

			if ( isset( $general_addons[0]['{{INDEX}}'] ) ) {
				unset( $general_addons[0]['{{INDEX}}'] );
			}

			if ( isset( $addons_meta ) && ! empty( $addons_meta ) ) {
				$addons = $addons_meta;
			} elseif ( ! ! $general_addons && in_array( $product_id, $general_addons['product_ids'] ) ) {
				$addons = $general_addons[0];
			}

			wc_get_template( 'addons-price.php', compact( 'addons', 'product' ), '', YITH_PA_DIR_TEMPLATES_PATH . '/' );
		}

		/**
		 * Add add-ons data to cart item
		 *
		 * @param  mixed $cart_item_data Custom data from cart.
		 * @param  mixed $cart_item_key  Custom data key from cart.
		 * @return $cart_item_data
		 */

		public function yith_add_addons_into_cart( $cart_item_data, $cart_item_key ) {
			error_log( print_r( $_POST, true ) );
			$product = $cart_item_data['data'];
			if ( $product->is_type( 'variation' ) ) {
				$variation_addons = $product->get_meta( 'yith_pa-addon' . $product->get_id() );
				if ( ! $variation_addons ) {
					$addons = $product->get_meta( 'yith_pa-addon' );
				} else {
					$addons = $variation_addons;
				}

				$addons         = ! ! $addons && is_array( $addons ) ? $addons : array();
				$parent_product = wc_get_product( $product->get_parent_id() );
				$parent_addons  = $parent_product->get_meta( 'yith_pa-addon' );
				$parent_addons  = ! ! $parent_addons && is_array( $parent_addons ) ? $parent_addons : array();
				$start_index    = count( $parent_addons ) + 1;

				foreach ( $addons as &$addon ) {

					$addon['index'] += $start_index;

				}
			} else {
				$addons = $product->get_meta( 'yith_pa-addon' );
			}

			if ( ! ! $addons && is_array( $addons ) ) {

				$addons_data = array();
				foreach ( $addons as &$addon ) {
					$index = $addon['index'];
					$name  = 'yith-pa-field-' . $index;

					if ( ! isset( $addon['enabled'] ) || 'yes' !== $addon['enabled'] || ! isset( $_POST[ $name ] ) && ! empty( $_POST[ $name ] ) ) {
						continue;
					}
					$label = $addon['name'];
					$text  = '';
					$price = 0;

					switch ( $addon['field_type'] ) {
						case 'text':
						case 'textarea':
							error_log( print_r( $_POST[ $name ], true ) );
							$input_text = sanitize_text_field( wp_unslash( $_POST[ $name ] ) );
							if ( ! $input_text ) {
								break;
							}
							$text = $input_text;
							if ( 'free' !== $addon['price_settings'] ) {
								$price = floatval( $this->yith_calculate_addon_text_price( $input_text, $addon ) );
							}
							break;
						case 'onoff':
						case 'checkbox':
							if ( 'yes' === $_POST[ $name ] ) {
								$text  = sanitize_text_field( $addon['description'] );
								$price = 'fixed' === $addon['price_settings'] ? floatval( $addon['price'] ) : 0;
							}
							break;
						case 'radio':
						case 'select':
							if ( ! isset( $_POST[ $name ] ) ) {
								break;
							}
							foreach ( $addon['options'] as $options => $option ) {
								$text = $option['name'];
								if ( 'fixed' === $addon['price_settings'] ) {
									$price = $option['price'];
								}
							}
							break;
					}

					if ( 'free' === $addon['price_settings'] ) {
						$price = 0;
					}
					if ( ! ! $text ) {
						$addons_data[] = compact( 'label', 'text', 'price' );
					}
				}

				$yith_pa                   = array(
					'addons'     => $addons_data,
					'base_price' => array(
						'text'  => wc_price( $product->get_price() ),
						'label' => __( 'Base price', 'yith-products-addons' ),
					),
				);
				$cart_item_data['yith_pa'] = apply_filters( 'yith_pa_add_cart_item_data', $yith_pa, $cart_item_data, $cart_item_key );
			}
			return $cart_item_data;
		}

		/**
		 * Calculates the price of the text based on the length of the string
		 *
		 * @param  mixed $input_text Text from custom input.
		 * @param  mixed $addon      Addon to take back the price options.
		 * @return void
		 */
		public function yith_calculate_addon_text_price( $input_text, $addon ) {
			$total_price = 0;
			$length      = strlen( $input_text );
			$free_char   = isset( $addon['free-char'] ) ? $addon['free-char'] : 0;
			$fixed_price = isset( $addon['price'] ) ? $addon['price'] : 0;

			if ( 'free' === $addon['price_settings'] ) {
				return;
			}
			if ( $length > $free_char ) {
				if ( 'fixed' === $addon['price_settings'] ) {

					$total_price += $fixed_price;

				} else {
					$total_price += ( $length - $free_char ) * $fixed_price;
				}
			}

			return $total_price;
		}

		/**
		 * Display on cart the addons data
		 *
		 * @param  mixed $item_data      Custom item data.
		 * @param  mixed $cart_item_data Cart data.
		 */
		public function yith_show_addons_in_cart( $item_data, $cart_item_data ) {
			if ( isset( $cart_item_data['yith_pa'] ) ) {
				$base_price  = $cart_item_data['yith_pa']['base_price'];
				$item_data[] = array(
					'key'   => esc_html( $base_price['label'] ),
					'value' => sanitize_text_field( $base_price['text'] ),
				);
				foreach ( $cart_item_data['yith_pa']['addons'] as $addon ) {
					if ( isset( $addon['label'] ) && isset( $addon['text'] ) && isset( $addon['price'] ) ) {
						$price       = $addon['price'];
						$item_data[] = array(
							'key'   => esc_html( $addon['label'] ),
							'value' => sanitize_text_field( $addon['text'] ) . '   + ' . wc_price( $price ),
						);
					}
				}
			}
			// error_log( print_r( $item_data, true ) );
			return $item_data;
		}

		/**
		 * Chande the product price based on the custom add ons
		 *
		 * @param  mixed $cart Object cart.
		 * @return void
		 */
		public function yith_add_addons_price_to_product( $cart ) {
			$total_price  = 0;
			$custom_price = 0;
			foreach ( $cart->get_cart() as $cart_item_key => $item ) {

				if ( isset( $item['yith_pa'] ) && ! empty( $item['yith_pa'] ) ) {

					foreach ( $item['yith_pa']['addons']  as $addons => $addon ) {

						$custom_price += $addon['price'];
					}
					$price       = $item['data']->get_price();
					$total_price = $price + $custom_price;
					if ( $price !== $total_price ) {
						$item['data']->set_price( $total_price );
					}
				}
			}
		}

		/**
		 * Add custom data to the order
		 *
		 * @param  mixed $item          Custom item data.
		 * @param  mixed $cart_item_key Key data from cart.
		 * @param  mixed $values        Custom values.
		 * @param  mixed $order         Order object.
		 * @return void
		 */
		public function yith_add_addons_data_in_order( $item, $cart_item_key, $values, $order ) {

			if ( isset( $values['yith_pa'] ) && ! empty( $values['yith_pa'] ) ) {

				foreach ( $values['yith_pa']['addons'] as $addons => $addon ) {

					$values = $addon['text'] . ' ( + ' . wc_price( $addon['price'] ) . ')';

					$item->add_meta_data(
						$addon['label'],
						$values,
						true
					);

				}
			}
		}

		/**
		 * AJAX call to print addons in variations products
		 *
		 * @return void
		 */
		public function yith_pa_ajax_variations_addons() {
			ob_start();
			if ( isset( $_POST['variation_id'] ) ) {
				$variation_id = intval( $_POST['variation_id'] );
				$product      = wc_get_product( $variation_id );
				$addons       = $product->get_meta( 'yith_pa-addon' );

				foreach ( $addons as $addon ) {
					$addon['product_id'] = $variation_id;
					$addon['index']     += $_POST['start_id'];
					if ( isset( $addon['enabled'] ) && 'yes' === $addon['enabled'] ) {
						wc_get_template( 'print-addon.php', compact( 'addon' ), '', YITH_PA_DIR_TEMPLATES_PATH . '/' );
					}
				}
			}
			wp_send_json( array( 'addons' => ob_get_clean() ) );
		}
	}
}
