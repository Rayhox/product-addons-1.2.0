<?php
/*
 * This file belongs to the YITH Products Addons.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

/**
 * Include templates
 *
 * @param $file_name name of the file you want to include.
 * @param array $args (array) (optional) Arguments to retrieve.
 */
if ( ! function_exists( 'yith_pa_get_template' ) ) {
	/**
	 * Yith_pa_get_template
	 *
	 * @param  mixed $file_name
	 * @param  mixed $args
	 * @return void
	 */
	function yith_pa_get_template( $file_name, $args = array() ) {
		extract( $args );
		$full_path = YITH_PA_DIR_TEMPLATES_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}

/**
 * Include views
 *
 * @param $file_name name of the file you want to include.
 * @param array $args (array) (optional) Arguments to retrieve.
 */
if ( ! function_exists( 'yith_pa_get_view' ) ) {
	/**
	 * Yith_pa_get_view
	 *
	 * @param  mixed $file_name
	 * @param  mixed $args
	 * @return void
	 */
	function yith_pa_get_view( $file_name, $args = array() ) {
		extract( $args );
		$full_path = YITH_PA_DIR_VIEWS_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}

if ( ! function_exists( 'print_price_on_addon' ) ) {

	/**
	 * Print the price html
	 *
	 * @param  mixed $price
	 * @param  mixed $plus
	 * @return void
	 */
	function print_price_on_addon( $price, $plus = false ) {
		$currency_symbol    = get_woocommerce_currency_symbol();
		$decimal_separator  = wc_get_price_decimal_separator();
		$thousand_separator = wc_get_price_thousand_separator();
		$decimals           = wc_get_price_decimals();
		$currency_position  = get_option( 'woocommerce_currency_pos', 'left' );
		$price              = number_format( $price, $decimals, $decimal_separator, $thousand_separator );

		$price_format = apply_filters( 'yith_pa_addon_price_format', 'left' === $currency_position ? '{plus}{currency} {price}' : '{plus}{price} {currency}' );
		$translate    = apply_filters(
			'yith_pa_addon_price_placeholders',
			array(
				'{currency}' => "<span class='yith-pa-price_currency'>{$currency_symbol}</span>",
				'{price}'    => "<span class='yith-pa-price'>{$price}</span>",
				'{plus}'     => $plus ? "<span class='yith-pa-price__plus'>+</span>" : '',
			)
		);
		$html = str_replace( array_keys( $translate ), $translate, $price_format );

		echo $html;
	}
}
