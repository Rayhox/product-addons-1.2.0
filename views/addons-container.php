<?php 
/**
 * Addons loop
 */
$default_values = array(
	'enabled'         => 'yes',
	'index'           => 0,
	'name'            => __( 'Untitled', 'yith-products-addons' ),
	'description'     => '',
	'field_type'      => 'text',
	'price_settings'  => 'free',
	'price'           => '0',
	'free_char'       => 0,
	'options'         => array(
		'name'  => '',
		'price' => '',
	),
	'default_enabled' => 'no',
);

$default_args = array( 'addon' => $default_values );
if ( isset( $loop ) ) {
	$default_args['loop'] = $loop + 1;
}
//error_log( print_r( $addons, true ) );
?>

<div class="yith-pa-total-container">
	<span class="yith-pa-add-new-addon"><?php esc_html_e( 'Add new Add-on', 'yith-products-addons' ); ?></span>
	<div class="yith-pa-template yith-pa-hidden">
		<?php yith_pa_get_view( '/addons-template.php', $default_args ); ?>
	</div>
	<div class="yith-pa-addons">
		<?php
		foreach ( $addons as $addon ) {

			$args = array( 'addon' => $addon );

			if ( isset( $loop ) ) {
				$args['loop'] = $loop + 1;
			}

			yith_pa_get_view( '/addons-template.php', $args );
		}
		?>
	</div>
</div>
