jQuery(document).ready(function ( $ ) {
    
    
    //TAKE THE CURRENT ADDON
    getAddon = function ( element ) {
        return element.closest( '.yith-pa-addon' );
    },

    //RETURN THE FORMATED PRICE WITH CORRECT SEPARATORS
    priceFormat = function ( price ) {
        price = String( price.toFixed( yithPa.decimalPrecision ) );

        if ( ',' === yithPa.decimalSeparator ) {
            price = price.replace( '.', ',' );
        }
        return price;
    },
    // SET THE PRICE ON LOAD
    initAddon = function ( addonContainer ) {
        addonContainer.find( '.yith-pa-addon__input' ).each( function() {
            if ( $( this ).is( 'select' ) || $( this ).is( 'input[type=radio]' ) ) {
                $( this ).trigger( 'change' );
            } else if ( $( this ).is( 'textarea' ) || $( this ).is( 'input[type=text]' ) ) {
                $( this ).trigger( 'keyup' );
            } else if ( $( this ).is( 'checkbox' ) || $( this ).is( 'input[type=checkbox]' ) ) {
                $( this ).trigger( 'change' );
            }
        } );
    },
    // SET THE PRICE OF THE TEXT & TEXTAREA ON KEYUP
    textChange = function() {
        var textInput      = $( this ),
            addonContainer = getAddon( textInput ),
            priceContainer = addonContainer.find( '.yith-pa-price__container' ),
            price          = priceContainer.find( '.yith-pa-price' ),
            priceValue     = getTextFieldPrice( $( this ) );

        if ( priceValue > 0 ) {
            priceContainer.slideDown();
            price.html( priceFormat( priceValue ) );
        } else {
            price.html( priceFormat( 0 ) );
            priceContainer.slideUp();
        }
    },
    // RETURN THE PRICE OF THE ADDON INPUT TEXT || TEXTAREA
    getTextFieldPrice = function ( textInput ) {
        var addonContainer  = getAddon( textInput ),
            priceSettings   = {
                settings:  addonContainer.data( 'price-settings' ),
                price:     parseFloat( addonContainer.data( 'price' ) ),
                freeChars: parseInt( addonContainer.data( 'free-chars' ) ),
            }
            textValueLength = textInput.val().length,
            priceValue      = 0;

        if ( textValueLength > priceSettings.freeChars ) {
            if ( 'fixed' === priceSettings.settings ) {
                priceValue = priceSettings.price;
                console.log('Entro fixed');
            } else {
                priceValue = ( textValueLength - priceSettings.freeChars ) * priceSettings.price;
                console.log('Entro no fixed');
            }
        }
        return priceValue;
    },
    //TRIGGERED WHEN RADIO || SELECT CHANGE
    optionChange = function() {
        var optionInput  = $( this ),
            addonContainer = getAddon( optionInput ),
            priceContainer = addonContainer.find( '.yith-pa-price__container' );

        if ( optionInput.val() > 0 ) {
            var price = priceContainer.find( '.yith-pa-price' );
            price.html( priceFormat( parseFloat( optionInput.val() ) ) );
        }
    },
    //TRIGGERED WHEN CHECKBOX || ONOFF CHANGE
    toggleChange = function() {
        var checkboxInput = $( this ),
            addonContainer = getAddon( checkboxInput ),
            priceContainer = addonContainer.find( '.yith-pa-price__container' );
            if ( checkboxInput.is( ':checked' ) ) {
                priceContainer.slideDown();
            } else {
                priceContainer.slideUp();
            }

    },
    //CALCULATES THE CURRENT PRICE OF THE ADDONS
    calculateAddonsPrice = function() {
        var addonsPrice = 0;
        $( '.yith-pa-addon__input' ).each( function () {
            var addonPrice = 0;
            if( $( this ).is( 'input[type=checkbox]' ) && $( this ).is( ':checked' ) ) {
                var addonContainer = getAddon( $( this ) ),
                    price          = addonContainer.attr('data-price');
                
                addonPrice = price !== 'undefined' ? parseInt( price ) : 0;

            } else if ( $( this ).is( 'input[type=text]' ) || $( this ).is( 'textarea' ) ) {

                addonPrice = getTextFieldPrice( $( this ) );
                // console.log( 'Entro desde textarea' );
            } else if ( $( this ).is( 'select' ) ) {

                var addonContainer     = getAddon( $( this ) ),
                    addonSelect        = addonContainer.find( '.yith-pa-field-select' ),
                    addonSelectedIndex = addonSelect.prop( 'selectedIndex' ),
                    addonPriceSettings = addonContainer.data( 'price-settings' ),
                    addonOptions       = addonContainer.data( 'options' );
                if ( 'free' === addonPriceSettings || typeof addonSelectedIndex === 'undefined' ) {
                    return;
                }

                addonPrice = parseFloat( addonOptions[addonSelectedIndex].price );

            } else if ( $( this ).is( 'input[type=radio]' ) && $( this ).is( ':checked' ) ) {
                var addonContainer     = getAddon( $( this ) ),
                    addonPriceSettings = addonContainer.data( 'price-settings' );

                if ( 'free' === addonPriceSettings ) {
                    return;
                }
                var addonRadioButtons = addonContainer.find( '.yith-pa-field--radio' ),
                    addonRadioIndex   = addonRadioButtons.index( $( this ) ),
                    addonOptions      = addonContainer.data( 'options' );
                
                addonPrice = parseFloat( addonOptions[addonRadioIndex].price );

            }
            addonsPrice += isNaN( addonPrice ) ? 0 : addonPrice;
        } );
        // console.log( addonPrice );
        $( '.yith-pa-addons-price__value' ).html( priceFormat( addonsPrice ) );
        calculateTotalPrice( addonsPrice );
    },

    //CALCULATES THE TOTAL PRICE OF THE PRODUCT BASED ON THE ADDONS
    calculateTotalPrice = function( addonsPrice ) {
        $( '.yith-pa-total-price__value .yith-pa-price' ).html( priceFormat( parseFloat( yithPa.productPrice ) + addonsPrice ) );
    },
    
    //PRODUCT VARIATION CHANGE
    variationChange = function( event, variation ) {
        var post_data = {
            variation_id: variation.variation_id,
            action:       'yith_pa_variation_addons',
            start_id:     $( '.yith-pa-addon' ).length + 1,
        },
        productPrice = $( '.yith-pa-total-price__value .yith-pa-price' );
        variationContainer = $( '.yith-pa-variation-addons' );
        yithPa.productPrice = variation.display_price;
        productPrice.html( priceFormat( yithPa.productPrice ) );
        $.ajax( {
            type: 'POST',
            dataType: 'json',
            data: post_data,
            url: yithPa.ajaxUrl,
            success: function ( response ) {
                var variationAddons = $( '.yith-pa-variation-addons' );
                variationAddons.html( response['addons'] );
                initAddon( variationAddons );
                variationContainer.slideDown( 'fast' );
            },
            complete: function() {
                calculateAddonsPrice();
            },
        } );

    };

    // VARIATION CLEAR TRIGGER
    variationClear = function() {
        var variationContainer = $( this ).find( '.yith-pa-variation-addons' );

        variationContainer.slideUp('fast');

    };
    
    // LISTENERS
                // SELECT & RADIO
    $( document ).
        on( 'change', '.yith-pa-field--radio:checked', optionChange ).
        on( 'change', '.yith-pa-field--radio:checked', calculateAddonsPrice );
    $( document ).
        on( 'change', '.yith-pa-field-select', optionChange ).
        on( 'change', '.yith-pa-field-select', calculateAddonsPrice );
                // CHECKBOX & ONOFF
    $( document ).
        on( 'change', '.yith-pa-field__onoff', toggleChange ).
        on( 'change', '.yith-pa-field__onoff', calculateAddonsPrice );
    $( document ).
        on( 'change', '.yith-pa-field__checkbox', toggleChange ).
        on( 'change', '.yith-pa-field__checkbox', calculateAddonsPrice );
                // TEXT & TEXTAREA
    $( document ).
        on( 'keyup', '.yith-pa-field--text', textChange ).
        on( 'keyup', '.yith-pa-field--text', calculateAddonsPrice );
                // VARIATION TRIGGER
    $( document ).
        on ( 'found_variation', variationChange );
                // VARIATION CLEAR
    $( document ).
        on( 'reset_data', variationClear );

    initAddon( $( '.yith-pa-addons' ) );
    calculateAddonsPrice();
});
